---
title: Eco Label
layout: eco-label
#menu:
#   main:
#     name: BE4FOSS
#     weight: 3
---
In 2020 the Umweltbundesamt ('German Environment Agency') released the award criteria for obtaining eco-certification with the Blauer Engel label for desktop software. Categories for certification include energy efficiency, extending the potential operating life of hardware, and user autonomy … all of which fit seamlessly with free and open source software.

The BE4FOSS project advances eco-certification for resource efficient software in the FOSS community. Obtaining the Blauer Engel label occurs in 3 steps: (1) Measure, (2) Analyze, and (3) Certify.
1. **Measure** in dedicated labs, such as at KDAB Berlin
2. **Analyze** using statistical tools such as OSCAR (Open source Software Consumption Analysis in R)
3. **Certify** by submitting the report on the fulfillment of the Blauer Engel criteria

The benefits of obtaining the ecolabel include:
- Recognition of reaching high standards for environmentally-friendly software design
- Differentiating free software from the alternatives
- Increasing the appeal of adoption for consumers
- Promoting transparency in the ecological footprint of software
