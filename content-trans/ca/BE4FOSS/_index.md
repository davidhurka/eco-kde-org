---
layout: eco-label
title: Etiqueta Eco
---
En 2020, la Umweltbundesamt («Agència Alemanya del Medi Ambient») va publicar els criteris de concessió per obtenir l'ecocertificació amb l'etiqueta Blauer Engel per al programari d'escriptori. Les categories de la certificació inclouen l'eficiència energètica, ampliació de la vida operativa potencial del maquinari, i l'autonomia de l'usuari… tot això s'adapta sense problemes al programari de codi lliure i obert.

El projecte BE4FOSS avança l'ecocertificació del programari eficient en recursos a la comunitat FOSS. L'obtenció de l'etiqueta Blauer Engel es fa en 3 passos: (1) Mesura, (2) Anàlisi, i (3) Certificació.

1. **Mesurar** en laboratoris dedicats, com el de KDAB de Berlín
1. **Anàlisi** usant eines estadístiques com l'OSCAR («Open source Software Consumption Analysis in R», Anàlisi de consum del programari de codi obert en R)
1. **Certificació** amb la presentació de l'informe sobre el compliment dels criteris de Blauer Engel

Els beneficis d'obtenir l'ecoetiqueta inclouen:

- Reconeixement de l'assoliment d'estàndards elevats en el disseny de programari respectuós amb el medi ambient
- Diferenciació del programari lliure de les alternatives
- Incrementar l'interès d'adopció per part dels consumidors
- Promocionar la transparència en la petjada ecològica del programari
