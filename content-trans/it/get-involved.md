---
menu:
  main:
    weight: 4
name: KDE Eco
title: Partecipa
userbase: KDE Eco
---
## Entra a far parte del movimento del software sostenibile

Abbiamo bisogno di quante più persone motivate per portare avanti questo progetto. Ecco alcuni canali in cui è possibile ottenere maggiori informazioni e contribuire.

### Comunica

- Stanza Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Lista di distribuzione: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Deposito GitLab FEEP: https://invent.kde.org/cschumac/feep
- Deposito GitLab BE4FOSS: https://invent.kde.org/cschumac/feep

## Discuti

- BigBlueButton: Incontri mensili, 2° mercoledì 17:00 UTC (contattaci per i dettagli)
- Lista di distribuzione di efficienza energetica: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Stanza Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org

## Supporto della comunità

- KDE Eco Forum: https://forum.kde.org/viewforum.php?f=334

## Contatto

Email: `joseph [at] kde.org`
