---
layout: energy-efficiency
menu: ''
title: Efficienza energetica
---
FEEP sta sviluppando strumenti per migliorare l'efficienza energetica nello sviluppo di software libero e open source. La progettazione e l'implementazione del software ha un impatto significativo sul consumo energetico dei sistemi di cui fa parte. Con gli strumenti giusti è possibile quantificare e abbattere i consumi energetici. Questa maggiore efficienza contribuisce a un uso più sostenibile dell'energia come una delle risorse condivise del nostro pianeta.
