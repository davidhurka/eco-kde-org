---
layout: energy-efficiency
menu: ''
title: Energetska učinkovitost
---
FEEP razvija orodja za izboljšanje energetske učinkovitosti razvoja proste in odprte programske opreme. Oblikovanje in izdelava programske opreme pomembno vplivata na porabo energije sistemov, katere so sestavni del. S pravimi orodji je mogoče količinsko opredeliti in si prizadevati za zmanjšanje porabe energije. Ta povečana učinkovitost prispeva k bolj trajnostni rabi energije kot enega od skupnih virov našega planeta.
