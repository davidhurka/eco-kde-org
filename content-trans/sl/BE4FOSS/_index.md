---
layout: eco-label
title: Eko značka
---
Leta 2020 je Umweltbundesamt ("Nemška agencija za okolje") objavila merila za nagrado za pridobitev eko-potrdila z oznako Modri angel za namizno programsko opremo. Kategorije za potrjevanje vključujejo energetsko učinkovitost, razširitev potencialne operativne življenjske dobe strojne opreme in avtonomije uporabnikov... Vse to se brez težav prilega prosti in odprtokodni programski opremi.

Projekt BE4FOSS predstavlja napredek pri potrjevanju učinkovite rabe virov pri skupnosti proste programske opreme. Pridobitev značke Modri angel poteka v treh korakih: (1) Meritve, (2) Analiza, in (3) Potrjevanje.

1. **Meritve** v za to namenjenih laboratorijih, kot je npr. KDAB Berlin
1. **Analiza** uporaba statističnih orodij kot npr. OSCAR (Open source Software Consumption Analysis in R)
1. **Potrjevanje** z vlogo poročila o zagotavljanju kriterijev za Modrega angela

Prednosti pridobitve eko značke vsebujejo:

- Priznanje doseganja visokih standardov pri oblikovanju okolju prijazne programske opreme
- Razlikovanje prostega programja od alternativnih programov
- Povečanje privlačnosti za rabo pri potrošnikih
- Spodbujanje preglednosti pri ekološkem odtisu programske opreme
