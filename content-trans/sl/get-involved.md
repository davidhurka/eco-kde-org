---
menu:
  main:
    weight: 4
name: KDE Eco
title: Bodite vpleteni
userbase: KDE Eco
---
## Postanite del gibanja za trajnostno programsko opremo

Potrebujemo kar največ motiviranih ljudi, ki bodo poganjali razvoj naprej. Tukaj je nekaj kanalov, kjer lahko dobite več informacij in lahko prispevate.

### Komunicirajte

- Matrix soba: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Poštni šeznam: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Repozitorij FEEP GitLab: https://invent.kde.org/cschumac/feep
- Repozitorij BE4FOSS GitLab: https://invent.kde.org/joseph/be4foss

## Debata

- BigBlueButton : Mesečni sestanki, druga sreda ob 17:00 UTC (stopite z nami v stik za podrobnosti)
- Poštni seznam energetske učinkovitosti: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Matrix soba: https://webchat.kde.org/#/room/#energy-efficiency:kde.org

## Podpora skupnosti

- KDE Eco Forum: https://forum.kde.org/viewforum.php?f=334

## Kontakt

E-pošta: `joseph [at] kde.org`
