---
menu:
  main:
    weight: 4
name: KDE Eco
title: Zapojiť sa
userbase: KDE Eco
---
## Become a part of the sustainable software movement

We need as many motivated people as possible to drive this forward. Here are some channels where you can get more information and contribute.

### Communicate

- Matrix room: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Mailing list: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- FEEP GitLab repository: https://invent.kde.org/cschumac/feep
- BE4FOSS GitLab repository: https://invent.kde.org/joseph/be4foss

## Discuss

- BigBlueButton : Monthly meet-ups, 2nd Wednesdays 17:00 UTC (contact us for details)
- Energy Efficiency Mailing List: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Matrix Room: https://webchat.kde.org/#/room/#energy-efficiency:kde.org

## Community Support

- KDE Eco Forum: https://forum.kde.org/viewforum.php?f=334

## Kontakt

Email: `joseph [at] kde.org`
