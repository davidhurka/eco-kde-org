---
menu:
  main:
    weight: 4
name: KDE Eco
title: Engagera dig
userbase: KDE Eco
---
## Bli en del av rörelsen för hållbar programvara

Vi behöver så många motiverade personer som möjligt för att komma framåt. Här är några kanaler där du kan få mer information och bidra.

### Kommunicera

- Matrix-rum: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- E-postlista: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- FEEP GitLab-arkiv: https://invent.kde.org/cschumac/feep
- BE4FOSS GitLab-arkiv: https://invent.kde.org/joseph/be4foss

## Diskutera

- BigBlueButton: Månadsvisa träffar, andra onsdagen 17:00 UTC (kontakta oss för detaljerad information)
- E-postlista för energieffektivitet: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Matrix-rum: https://webchat.kde.org/#/room/#energy-efficiency:kde.org

## Gemenskapsstöd

- KDE miljöforum: https://forum.kde.org/viewforum.php?f=334

## Kontakter

E-post: `joseph [snabela] kde.org`
