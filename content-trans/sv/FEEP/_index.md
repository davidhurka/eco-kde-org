---
layout: energy-efficiency
menu: ''
title: Energieffektivitet
---
FEEP utvecklar verktyg för att förbättra energieffektivitet i fri och öppen programvaruutveckling. Konstruktion och implementering av programvara har en betydande påverkan på energiförbrukningen i systemet som den ingår i. Med rätt verktyg är det möjligt att kvantifiera och reducera energiförbrukning. Den ökade effektiviteten bidrar till en mer hållbar energianvändning, såsom en av vår planets gemensamma resurser.
