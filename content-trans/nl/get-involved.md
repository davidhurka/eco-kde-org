---
menu:
  main:
    weight: 4
name: KDE Eco
title: Doe mee
userbase: KDE Eco
---
## Wordt een onderdeel van de duurzame software beweging

We hebben zoveel mogelijk personen nodig om dit vooruit te brengen. Hier zijn enige kanalen waar u meer informatie kan krijgen en bijdragen.

### Communicatie

- Matrix-room: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- E-maillijst: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- FEEP GitLab opslagruimte: https://invent.kde.org/cschumac/feep
- BE4FOSS GitLab opslagruimte: https://invent.kde.org/joseph/be4foss

## Discussie

- BigBlueButton : Maandelijkse ontmoetingen, tweede woensdag 17:00 UTC (neem contact op met ons voor details)
- Energie-efficiëntie e-maillijst: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Matrix-room: https://webchat.kde.org/#/room/#energy-efficiency:kde.org

## Ondersteuning door de gemeenschap

- KDE Eco-forum: https://forum.kde.org/viewforum.php?f=334

## Contact

E-mail: `joseph [at] kde.org`
