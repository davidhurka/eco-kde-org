---
layout: eco-label
title: Eco-label
---
In 2020 heeft het Umweltbundesamt ('Duits milieu agentschap') de criteria voor het verkrijgen van de prijs voor eco-certificatie met het label Blauer Engel voor bureaubladsoftware uitgegeven. Categorieën voor certificeren omvatten energie-efficiëntie, de potentiële werkbare leeftijd van hardware en gebruikersautonomie … allemaal zaken die naadloos passen bij vrije en open-source software.

Het BE4FOSS project brengt voortgang in eco-certificatie voor efficiëntie in hulpbronnen voor software in de FOSS gemeenschap. Het label Blauer Engel verkrijgen gebeurt in drie stappen: (1) Meten, (2) Analyseren en (3) Certificeren.

1. **Meten** in gespecialiseerde labs, zoals bij KDAB Berlin
1. **Analyseren** met statistische hulpmiddelen zoals OSCAR (Open source Software Consumption Analysis in R)
1. **Certificeren** door indienen van het rapport over het vervullen van de criteria voor Blauer Engel

De voordelen van het verkrijgen van het ecolabel omvatten:

- Herkenning van het bereiken van hoge standaarden voor milieu-vriendelijk ontwerpen van software
- Onderscheid maken tussen vrije software en de alternatieven
- Verhogen van het appel op adoptie door consumenten
- Transparantie promoten in de ecologische voetafdruk van software
