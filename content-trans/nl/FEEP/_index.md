---
layout: energy-efficiency
menu: ''
title: Energie-efficiëntie
---
FEEP is het ontwikkelen van hulpmiddelen om energie-efficiëntie te verbeteren in het ontwikkelen van vrije en open-source software. Ontwerp en implementatie van software heeft een significante invloed op de energieconsumptie van de systemen waar het onderdeel van is. Met de juiste hulpmiddelen is het mogelijk energieconsumptie te kwantificeren en omlaag te brengen. Deze verhoogde efficiëntie draagt bij aan een meer duurzaam gebruik van energie als een van de gedeelde hulpbronnen van onze planeet.
