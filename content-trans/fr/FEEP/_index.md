---
layout: energy-efficiency
menu: ''
title: Efficacité énergétique
---
Le projet « FEEP » développe des outils pour améliorer l'efficacité énergétique dans le développement de logiciels libres et « Open source ». La conception et la réalisation de logiciels ont un impact significatif sur la consommation d'énergie des systèmes dont ils font partie. Avec les bons outils, il est possible de quantifier et de réduire la consommation d'énergie. Cette efficacité accrue contribue à une utilisation plus durable de l'énergie, l'une des ressources partagées de notre planète.
