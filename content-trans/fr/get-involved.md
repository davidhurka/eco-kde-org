---
menu:
  main:
    weight: 4
name: KDE Eco
title: Impliquez-vous
userbase: KDE Eco
---
## Devenir une partie du mouvement pour des logiciels durables

Nous avons besoin du plus grand nombre possible de personnes motivées pour faire avancer ce projet. Voici quelques canaux où vous pouvez obtenir plus d'informations et contribuer.

### Communiquer

- Salon « Matrix » : https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Liste de diffusion : https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Dépôt « FEEP GitLab » : https://invent.kde.org/cschumac/feep
- Dépôt « BE4FOSS GitLab » : https://invent.kde.org/joseph/be4foss

## Discuter

- BigBlueButton : réunions mensuelles, chaque second mercredi à 17:00 UTC (contactez nous pour plus de détails)
- Liste de diffusion sur l'efficacité énergétique : https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Salon « Matrix » : https://webchat.kde.org/#/room/#energy-efficiency:kde.org

## Prise en charge de la communauté

- Forum de KDE Eco : https://forum.kde.org/viewforum.php?f=334

## Contact

Courriel : « joseph [at] kde.org »
