---
layout: eco-label
title: Label Eco
---
En 2020,l'agence allemande pour l'environnement « Umweltbundesamt » a publié les critères d'attribution pour l'obtention de l'écocertification avec le label « Blauer Engel » pour les logiciels de bureau. Parmi les catégories de certification figurent l'efficacité énergétique, l'allongement de la durée de vie potentielle du matériel et l'autonomie de l'utilisateur... autant d'éléments qui s'intègrent parfaitement aux logiciels libres et « Open source ».

Le projet « BE4FOSS » fait progresser l'éco-certification des logiciels économes en ressources dans la communauté « FOSS ». L'obtention du label « Blauer Engel » se réaliser selon 3 étapes : (1) Mesurer, (2) Analyser, et (3) Certifier.

1. ** Mesurer ** dans des laboratoires spécifiques, tels que le KDAB de Berlin
1. ** Analyser ** grâce à l'utilisation d'outils de statistiques comme OSCAR, un logiciel libre développé en langage « R » pour l'analyse de la consommation énergétique (« Open source Software Consumption Analysis in R »)
1. ** Certifier ** en diffusant le rapport concernant le respect des critères du certificat « Blauer Engel ».

Les bénéfices de l'obtention de ce label Éco concernent :

- Reconnaître le besoin d'atteindre des normes élevées en matière de conception de logiciels respectueux de l'environnement
- Différencier les logiciels libres de leurs alternatives
- Augmenter l'attrait de l'adoption vis à vis des consommateurs
- Promouvoir la transparence sur l'empreinte écologique des logiciels
