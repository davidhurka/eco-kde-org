module eco-kde-org

go 1.15

require (
	github.com/gohugoio/hugo-mod-bootstrap-scss-v4 v1.0.0 // indirect
	github.com/thednp/bootstrap.native v0.0.0-20210203161624-d9ba45b5ee95 // indirect
	github.com/twbs/bootstrap v4.6.0+incompatible // indirect
	invent.kde.org/websites/aether-sass v0.0.0-20211104180455-9212dc6ddd6f // indirect
)
